﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Issoft.Meetups.Singletons
{
    class SingletonWithoutStaticConstructor
    {
        public static readonly SingletonWithoutStaticConstructor Instance = new SingletonWithoutStaticConstructor();

        private SingletonWithoutStaticConstructor()
        {
            Console.WriteLine("Constructing SingletonWithoutStaticConstructor");
        }
    }

    class SingletonWithStaticConstructor
    {
        public static readonly SingletonWithStaticConstructor Instance = new SingletonWithStaticConstructor();

        private SingletonWithStaticConstructor()
        {
            Console.WriteLine("Constructing SingletonWithStaticConstructor");
        }

        static SingletonWithStaticConstructor()
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Singletons Sample...");

            var not1 = Convert.ToInt32("2");

            if (not1 == 1)
            {
                var s1 = SingletonWithoutStaticConstructor.Instance;
                var s2 = SingletonWithStaticConstructor.Instance;
            }

            Console.WriteLine("Like it?");
            Console.ReadLine();
        }
    }
}
